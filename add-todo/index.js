const Redis = require("ioredis");
const redisUrl = "todo-app-db2.yfzxbo.0001.eun1.cache.amazonaws.com:6379";

exports.handler = async(input) => {
    if (!input) {
        return {
            "statusCode": 400,
            "body": "no body input is not defined"
        }
    }
    if (!input.todo) {
        return {
            "statusCode": 400,
            "body": "todo is not defined"
        }
    }
    if (!input.user) {
        return {
            "statusCode": 400,
            "body": "user is not defined"
        }
    }

    var client = new Redis(redisUrl);
    await client.rpush(input.user, input.todo);
    await client.disconnect();
    
    return {
        "statusCode": 200
    };
};