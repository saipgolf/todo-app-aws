# Web API (todo-app) in Amazon Web Services 

To deploy it to Amazon, the following steps must be done.

1. Register an account with Amazon, make sure to create all resources in same region.
2. Create a Virtual Private Cloud (VPC).
3. Import the 6 functions from the repository and assign the functions to the VPC.
4. Create a ElastiCache (Redis-variant) and assign it to the same VPC.
5. Finally create an Amazon API Gateway (REST-variant), and make it direct calls to the functions:
    - GET /about            -> about
    - GET /credit           -> credit
    - POST /todo            -> add-todo
    - GET /todo?user={user} -> list-todo
    - DELETE /todo          -> clear-todo
6. Create a policy named InvokeLambda:
    - Add the following permissions to it: lambda:InvokeFunction, lambda:InvokeAsync
    - Assign the policy to the "about"-function
7. Create a policy named LambdaNetworkPolicy:
    - Add the following permissions to it: ec2:DescribeNetworkInterfaces, 
        ec2:CreateNetworkInterface, ec2:DeleteNetworkInterface, 
        ec2:DescribeInstances, ec2:AttachNetworkInterface
    - Assign the policy to all functions that access the ElastiCache (add-todo, list-todo, clear-todo)

## Example of usage:
curl -X GET https://cgov2zn34h.execute-api.eu-north-1.amazonaws.com/prod/about

curl -X GET https://cgov2zn34h.execute-api.eu-north-1.amazonaws.com/prod/credit

curl -X POST -d '{"user":"user1","todo":"a"}' https://cgov2zn34h.execute-api.eu-north-1.amazonaws.com/prod/todo

curl -X GET https://cgov2zn34h.execute-api.eu-north-1.amazonaws.com/prod/todo?user=user1

curl -X DELETE -d '{"user":"user1","todo":"a"}' https://cgov2zn34h.execute-api.eu-north-1.amazonaws.com/prod/todo