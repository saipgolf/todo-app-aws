let AWS = require('aws-sdk');
let lambda = new AWS.Lambda();

exports.handler = async (event) => {

    let params = {
      FunctionName: 'about-internal',
      InvocationType: 'RequestResponse'
    };

    return await lambda.invoke(params, function(err, data) {
      if (err) {
        throw err;
      }
      return data.Payload;
    }).promise().then(data => {
        var data = data.Payload;
        var object = JSON.parse(data);
        return object.body;
    });
};