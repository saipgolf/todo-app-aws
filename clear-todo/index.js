var Redis = require("ioredis");

exports.handler = async(input) => {
    if (!input) {
        return {
            "statusCode": 400,
            "body": "no body input is not defined"
        }
    }
    if (!input.user) {
        return {
            "statusCode": 400,
            "body": "user is not defined"
        }
    }

    var client = new Redis("todo-app-db2.yfzxbo.0001.eun1.cache.amazonaws.com:6379");
    
    await client.del(input.user);
    
    await client.disconnect();
    
    return {
        "statusCode": 200
    };
};