var Redis = require("ioredis");

exports.handler = async (event) => {
    var user = event["queryStringParameters"]['user'];
    if (!user) {
        return {
            "statusCode": 400,
            "body": "user query param is missing"
        };
    }
    
    var client = new Redis("todo-app-db2.yfzxbo.0001.eun1.cache.amazonaws.com:6379");
    
    var data = await client.lrange(user, 0, 1000);
    
    await client.disconnect();

    return {
        "statusCode": 200,
        "body": JSON.stringify({"user": user, "todo": data})
    };
};