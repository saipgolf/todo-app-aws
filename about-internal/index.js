exports.handler = async (event) => {
    const response = {
        statusCode: 200,
        body: JSON.stringify('about-internal: Sample application created by Team Golf'),
    };
    
    return response;
};
